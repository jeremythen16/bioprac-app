import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'

import Home from './components/Home';
import Practice from './components/Practice';
import Notfound from './components/Notfound';

function App() {
  return (
    <Router>
      <div className="App">
      <NavBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/practice" component={Practice} />
        <Route component={Notfound} />
      </Switch>
      </div>
    </Router>
    
  );
}

class NavBar extends React.Component {

  render() {
    return (
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/practice">Practice</Link>
        </li>
      </ul>
    )
  }

}

export default App;
