import React, { useState, useEffect } from 'react';



function Practice() {

    const [questions, setQuestions] = useState([]);

    useEffect(() => {
        fetch('/api/questions')
            .then(response => response.json())
            .then(data => {
                console.log('data', data)
                setQuestions(data);
            });
    })


    return (
        
        <div>
            <div>Área de práctica</div>

            <div>Questions:</div>

            {
                questions.map(question => {
                    const { id, category, author, questionString, correctAnswer } = question;
                    const questionInfo = `id: ${id}, category: ${category}, author: ${author}, question: ${questionString}, correct answer: ${correctAnswer}`;
                    return <p>{questionInfo}</p>
                })
            }

        </div>
        



    );
}

export default Practice;